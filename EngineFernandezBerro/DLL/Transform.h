#pragma once
#include "Component.h"

class DLL_API Transform : public Component
{
private:
	glm::vec3 posicion;
	glm::vec3 rotacion;
	glm::vec3 escala;

	glm::mat4 matrizMundo;
	glm::mat4 matrizTraslacion;
	glm::mat4 matrizRotacion;
	glm::mat4 matrizEscala;

public:
	Transform(Renderer* rend);
	~Transform();
	void Update() override;
	void Start() override;
	void Draw(Renderer* rend) override;

	//SETTERS
	void SetPosicion(float x, float y, float z);
	void SetRotacion(float x, float y, float z);
	void SetEscala(float x, float y, float z);

	//GETTERS
	glm::vec3 GetPosicion();
	glm::vec3 GetRotacion();
	glm::vec3 GetEscala();
	glm::mat4 GetWorldMatrix();
};

