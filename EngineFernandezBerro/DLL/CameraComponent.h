#pragma once
#include "Export.h"
#include "Component.h"
#include "Transform.h"

class DLL_API CameraComponent :
	public Component
{
public:
	CameraComponent(Renderer * renderer);
	~CameraComponent();

	void Draw(Renderer* rend) override;
	void Update() override;
	void Start() override;

	void Walk(float dir);
	void Strafe(float dir);
	void Pitch(float deg);
	void Roll(float deg);
	void Yaw(float deg);

	Transform* transform;
	Renderer* rend;

private:
	glm::vec3 up;
	glm::vec3 forward;
	glm::vec3 right;
	glm::mat4 rot;
};

