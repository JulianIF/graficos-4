#include "BoxCollider.h"



BoxCollider::BoxCollider()
{
	isTrigger = false;
	isStatic = false;
}


BoxCollider::~BoxCollider()
{
}

void BoxCollider::SetPos(float _posX, float _posY)
{
	position[0] = _posX;
	position[1] = _posY;
}

void BoxCollider::SetSize(int _h, int _w)
{
	height = _h;
	width = _w;
}
