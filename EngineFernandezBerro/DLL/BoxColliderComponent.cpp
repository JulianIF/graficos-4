#include "BoxColliderComponent.h"



BoxColliderComponent::BoxColliderComponent(Renderer* rend):Component(rend)
{
	isTrigger = false;
	isStatic = false;
}


BoxColliderComponent::~BoxColliderComponent()
{
}

void BoxColliderComponent::SetPos(float _posX, float _posY)
{
	position[0] = _posX;
	position[1] = _posY;
}

void BoxColliderComponent::SetSize(int _h, int _w)
{
	height = _h;
	width = _w;
}
