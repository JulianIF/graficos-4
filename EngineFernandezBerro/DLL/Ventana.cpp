#include "Ventana.h"
#include <GLFW\glfw3.h>


Ventana::Ventana()
{

}


Ventana::~Ventana()
{
}

bool Ventana::Start(int w, int h, const char* n) 
{
	//Inicializar GLFW

	/*Initialize the library*/
	if (!glfwInit())
		return -1;

	/*Create a window and create its OpenGL context*/
	ventana = glfwCreateWindow(w, h, n, NULL, NULL);

	if (ventana == NULL) 
	{
		cout << "Failed to open GLFW window" << endl;
		glfwTerminate();
		return false;
	}

	return true;
}

bool Ventana::Stop() 
{
	if (ventana != NULL)
		glfwDestroyWindow((GLFWwindow*)ventana);
	ventana = NULL;

	glfwTerminate();

	return true;
}

bool Ventana::ShouldClose() 
{
	if (ventana)
		return glfwWindowShouldClose((GLFWwindow*)ventana);

	return true;
}

void Ventana::PollEvents() 
{
	glfwPollEvents();
}

void* Ventana::GetVentana() 
{
	return ventana;
}
